# Quick zabbix deployment

Project for quick Zabbix deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy Zabbix on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS. (except `proxy`)

Dependence
----------
* **CentOS**
  * sudo
* **Ubuntu**
  * gpg
  * PyMySQL (python package only for `proxy`)

Project's playbook
------------------

* **server.yml**: This playbook deploys latest Zabbix Server version on all hosts from
selected inventory.

* **client.yml**: This playbook deploys latest Zabbix Client version on all hosts from
selected inventory.

* **proxy.yml**: This playbook deploy deploy Zabbix Proxy on all hosts from selected inventory.

Enjoy it!